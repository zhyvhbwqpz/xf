package minesweeper;

import java.util.ArrayList;
import java.util.List;

public class MineSweeperImpl implements MineSweeper {

    private static final Character MINE_CHARACTER = '*';
    private static final String LINE_SEPARATOR = "\n";
    private final MineFieldValidator mineFieldValidator = new MineFieldValidator();

    private Dimension fieldDimension = new Dimension(0, 0);
    private List<Mine> mineList = new ArrayList<>();

    public void setMineField(String mineField) throws IllegalArgumentException {
        if (!mineFieldValidator.validate(mineField)) {
            throw new IllegalArgumentException("Mine field is not valid");
        }

        this.fieldDimension = getFieldDimension(mineField);
        this.mineList = getMineList(mineField);
    }

    public String getHintField() throws IllegalStateException {
        if (fieldDimension.getSquare() == 0) {
            throw new IllegalStateException("Mine field is not set");
        }

        return getFieldWithHints();
    }

    private String getFieldWithHints() {
        StringBuilder field = new StringBuilder();

        for (int x = 0; x < fieldDimension.getHeight(); x++) {
            for (int y = 0; y < fieldDimension.getWidth(); y++) {
                if (isMine(x, y)) {
                    field.append(MINE_CHARACTER);
                } else {
                    long hint = getHint(x, y);
                    field.append(hint);
                }
            }
            field.append(LINE_SEPARATOR);
        }

        return field.toString().trim();
    }

    private long getHint(int x, int y) {
        return mineList.stream().filter(v -> v.isNeighbouring(x, y)).count();
    }

    private boolean isMine(int x, int y) {
        return mineList.stream().anyMatch(v -> v.getX() == x && v.getY() == y);
    }

    private Dimension getFieldDimension(String mineField) {
        String[] rows = mineField.split(LINE_SEPARATOR);
        return new Dimension(rows[0].length(), rows.length);
    }

    private List<Mine> getMineList(String mineField) {
        List<Mine> mines = new ArrayList<>();
        String[] rows = mineField.split(LINE_SEPARATOR);

        for (int x = 0; x < fieldDimension.getHeight(); x++) {
            for (int y = 0; y < fieldDimension.getWidth(); y++) {
                if (MINE_CHARACTER.equals(rows[x].charAt(y))) {
                    mines.add(new Mine(x, y));
                }
            }
        }

        return mines;
    }
}
