package minesweeper;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

class MineFieldValidator {
    private static final Pattern CORRECT_CHARACTERS_REGEX = Pattern.compile("^[\\.|\\*|\\n]+$");
    private static final String LINE_SEPARATOR = "\n";

    boolean validate(String mineField) {
        return mineField != null
                && !mineField.isEmpty()
                && hasEqualRows(mineField)
                && hasCorrectCharacters(mineField);
    }

    private boolean hasEqualRows(String mineField) {
        List<String> rows = Arrays.asList(mineField.split(LINE_SEPARATOR));
        return rows.stream().skip(1).noneMatch(v -> v.length() != rows.get(0).length());
    }

    private boolean hasCorrectCharacters(String mineField) {
        return CORRECT_CHARACTERS_REGEX.matcher(mineField).matches();
    }
}
