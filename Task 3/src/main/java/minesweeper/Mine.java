package minesweeper;

import java.awt.*;

class Mine extends Point {

    Mine(int x, int y) {
        super(x, y);
    }

    boolean isNeighbouring(int x2, int y2) {
        double deltaX = Math.abs(getX() - x2);
        double deltaY = Math.abs(getY() - y2);

        return deltaX <= 1 && deltaY <= 1 && deltaX + deltaY <= 2;
    }
}