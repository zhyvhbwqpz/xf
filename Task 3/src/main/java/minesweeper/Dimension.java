package minesweeper;

class Dimension extends java.awt.Dimension {

    Dimension(int width, int height) {
        super(width, height);
    }

    int getSquare() {
        return width * height;
    }
}
