package minesweeper;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class MineFieldValidatorUnitTest {

    private MineFieldValidator mineFieldValidator = new MineFieldValidator();

    public Object[][] parametersForIsValid() {
        return new Object[][]{
                {null, false},
                {"***notvalid**", false},
                {"**\n*", false},
                {"*\n**", false},
                {".....", true},
                {"..**...", true},
                {"******", true},
                {"**\n**\n..", true},
        };
    }

    @Test
    @Parameters
    public void isValid(String mineField, boolean isValid) throws Exception {
       assertEquals(mineFieldValidator.validate(mineField), isValid);
    }
}
