package minesweeper;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class MineSweeperImplUnitTest {

    private MineSweeper mineSweeper;

    @Before
    public void initMineSweeper() {
        this.mineSweeper = new MineSweeperImpl();
    }

    public Object[][] parametersForExpectedHints() {
        return new Object[][]{
                {"***", "***"},
                {"...", "000"},
                {".*.", "1*1"},
                {"**.", "**1"},
                {"...\n.*.\n...", "111\n1*1\n111"},
                {"...\n.*.\n.*.", "111\n2*2\n2*2"},
        };
    }

    @Test
    @Parameters
    public void expectedHints(String mindField, String expectedHints) throws Exception {
        mineSweeper.setMineField(mindField);
        assertEquals(mineSweeper.getHintField(), expectedHints);
    }

    public Object[] parametersForThrowsIllegalArguemnt() {
            return new Object[]{
                    null,
                    "***notvalid**",
                    "**\n*",
                    "*\n**",
            };
        }

    @Parameters
    @Test(expected = IllegalArgumentException.class)
    public void throwsIllegalArguemnt(String mindField) throws Exception {
        mineSweeper.setMineField(mindField);
    }

    @Test(expected = IllegalStateException.class)
    public void throwsIllegalState() throws Exception {
        mineSweeper.getHintField();
    }
}
