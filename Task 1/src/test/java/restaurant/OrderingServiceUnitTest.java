package restaurant;

import model.Cuisine;
import model.Item;
import model.ItemCategory;
import model.Order;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import repository.ItemRepository;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class OrderingServiceUnitTest {

    private final static List<Item> ITEMS = Arrays.asList(
            new Item("main", "desc1", ItemCategory.MAIN_MEAL, Cuisine.POLISH, 1),
            new Item("name2", "desc2", ItemCategory.MAIN_MEAL, Cuisine.MEXICAN, 2),
            new Item("dessert", "desc1", ItemCategory.DESSERT, Cuisine.POLISH, 1),
            new Item("cola", "desc1", ItemCategory.DRINK, Cuisine.POLISH, 1),
            new Item("lemon", "desc1", ItemCategory.ADDITIVE, Cuisine.POLISH, 1)
    );

    @Mock
    private ItemRepository itemRepository;

    @Mock
    private InputService inputService;

    @InjectMocks
    private OrderingService orderingService;

    @Test
    public void orders() throws Exception {
        doReturn(ITEMS).when(itemRepository).getItems();
        when(inputService.getChoice(any()))
                .thenReturn(Cuisine.POLISH)
                .thenReturn(ItemCategory.MAIN_MEAL)
                .thenReturn(ITEMS.get(0))
                .thenThrow(InterruptedException.class);

        Order order = orderingService.getOrder();

        assertThat(order.getItemList()).containsExactly(ITEMS.get(0));
    }

    @Test
    public void ordersMultiple() throws Exception {
        doReturn(ITEMS).when(itemRepository).getItems();
        when(inputService.getChoice(any()))
                .thenReturn(Cuisine.POLISH)
                .thenReturn(ItemCategory.MAIN_MEAL)
                .thenReturn(ITEMS.get(0))
                .thenReturn(ItemCategory.DESSERT)
                .thenReturn(ITEMS.get(2))
                .thenReturn(ItemCategory.DRINK)
                .thenReturn(ITEMS.get(3))
                .thenReturn(ItemCategory.ADDITIVE)
                .thenReturn(ITEMS.get(4))
                .thenThrow(InterruptedException.class);

        Order order = orderingService.getOrder();

        assertThat(order.getItemList()).containsExactly(ITEMS.get(0), ITEMS.get(2), ITEMS.get(3), ITEMS.get(4));
    }
}
