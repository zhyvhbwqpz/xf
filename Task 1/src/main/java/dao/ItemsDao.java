package dao;

import model.Item;

import java.util.List;

public interface ItemsDao {
    List<Item> getItems();
}
