package model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Cuisine {
    POLISH(100, "Polish"),
    MEXICAN(200, "Mexican"),
    ITALIAN(300, "Italian");

    private final int id;
    private final String name;

    @Override
    public String toString() {
        return name;
    }
}
