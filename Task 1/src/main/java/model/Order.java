package model;

import lombok.Data;
import lombok.Value;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

@Data
public class Order {
    List<Item> itemList = new ArrayList<>();

    public int getPrice() {
        return itemList.stream().mapToInt(Item::getPrice).sum();
    }

    public boolean isEmpty() {
        return itemList.isEmpty();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        IntStream.range(0, itemList.size()).forEach(i -> stringBuilder.append(i + ". " + itemList.get(i) + "\n"));
        return stringBuilder.toString();
    }
}
