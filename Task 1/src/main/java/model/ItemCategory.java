package model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
public enum ItemCategory {
    MAIN_MEAL(100, "Main meal"),
    DESSERT(200, "Dessert"),
    DRINK(300, "Drink"),
    ADDITIVE(400, "Additive");

    private final int id;
    private final String name;

    @Override
    public String toString() {
        return name;
    }
}
