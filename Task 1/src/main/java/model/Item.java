package model;

import lombok.Value;

@Value
public class Item {
    String name;
    String description;
    ItemCategory itemCategory;
    Cuisine cuisine;
    int price;

    @Override
    public String toString() {
        return name + " [$" + price + "] " + description;
    }
}
