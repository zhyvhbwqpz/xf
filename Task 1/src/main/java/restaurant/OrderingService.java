package restaurant;

import model.Cuisine;
import model.Item;
import model.ItemCategory;
import model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.ItemRepository;

import java.util.ArrayList;
import java.util.List;

import static utils.ItemsUtils.getAvailableCategories;
import static utils.ItemsUtils.getAvailableCuisines;
import static utils.ItemsUtils.getItems;

@Service
public class OrderingService {

    private final List<Item> AVAILABLE_ITEMS;
    private InputService inputService;

    @Autowired
    public OrderingService(ItemRepository itemRepository, InputService inputService) {
        this.inputService = inputService;
        this.AVAILABLE_ITEMS = itemRepository.getItems();
    }

    Order getOrder() {
        Order order = new Order();

        try {
            Cuisine cuisine = chooseCuisine();
            List<Item> orderedItems = getOrderItemList(cuisine);
            order.getItemList().addAll(orderedItems);
        } catch (InterruptedException ex) {
            //DO NOTHING
        }

        return order;
    }

    private List<Item> getOrderItemList(Cuisine cuisine) {
        List<Item> itemsOrdered = new ArrayList<>();

        while (true) {
            try {
                ItemCategory itemCategory = chooseCategoryFromCuisine(cuisine);

                List<Item> categoryItems = getItems(AVAILABLE_ITEMS, cuisine, itemCategory);
                Item item = chooseItem(categoryItems);
                itemsOrdered.add(item);
            } catch (InterruptedException ex) {
                break;
            }
        }

        return itemsOrdered;
    }

    private Cuisine chooseCuisine() throws InterruptedException {
        List<Cuisine> cuisines = getAvailableCuisines(AVAILABLE_ITEMS);
        return inputService.getChoice(cuisines);
    }

    private ItemCategory chooseCategoryFromCuisine(Cuisine cuisine) throws InterruptedException {
        List<Item> cuisineItems = getItems(AVAILABLE_ITEMS, cuisine);
        List<ItemCategory> categories = getAvailableCategories(cuisineItems);
        return inputService.getChoice(categories);
    }

    private Item chooseItem(List<Item> items) throws InterruptedException {
        return inputService.getChoice(items);
    }
}
