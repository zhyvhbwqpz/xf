package restaurant;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;

@Service
class InputService {
    <T> T getChoice(List<T> items) throws InterruptedException {
        Scanner reader = new Scanner(System.in);
        System.out.println("Which item would you like to choose?");
        IntStream.range(0, items.size()).forEach(i -> System.out.println(i + ". " + items.get(i)));
        int cancelOrderItemId = items.size();
        System.out.println(cancelOrderItemId + ". That's enough");

        int itemId = 0;
        do {
            System.out.print("You choose item nr: ");
            itemId = reader.nextInt();
        } while (itemId < 0 || itemId > cancelOrderItemId);

        if (itemId == cancelOrderItemId) {
            throw new InterruptedException("That's enough");
        }

        T item = items.get(itemId);
        System.out.println("Choose " + item);
        return item;
    }
}
