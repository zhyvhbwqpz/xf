package restaurant;

import model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WaiterService {

    private OrderingService orderingService;

    @Autowired
    public WaiterService(OrderingService orderingService) {
        this.orderingService = orderingService;
    }

    void startOrdering() {
        Order order = orderingService.getOrder();

        if (!order.isEmpty()) {
            System.out.println("Your order costs $" + order.getPrice() + ": \n" + order);
        } else {
            System.out.println("We are sorry that our products didn't suit you");
        }
    }

}
