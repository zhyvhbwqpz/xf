package repository;

import dao.ItemsDao;
import model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ItemRepository {

    private ItemsDao itemsDao;

    @Autowired
    public ItemRepository (ItemsDao itemsDao) {
        this.itemsDao = itemsDao;
    }

    public List<Item> getItems() {
        return itemsDao.getItems();
    }
}
