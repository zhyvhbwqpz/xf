package utils;

import lombok.NonNull;
import lombok.experimental.UtilityClass;
import model.Cuisine;
import model.Item;
import model.ItemCategory;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

@UtilityClass
public class ItemsUtils {

    public static List<Cuisine> getAvailableCuisines(List<Item> items) {
        return items.stream().map(Item::getCuisine).collect(toList());
    }

    public static List<ItemCategory> getAvailableCategories(List<Item> items) {
        return items.stream().map(Item::getItemCategory).collect(toList());
    }

    public static List<Item> getItems(List<Item> items, Cuisine cuisine) {
        return items.stream().filter(v -> cuisine == v.getCuisine()).collect(toList());
    }

    public static List<Item> getItems(List<Item> items, ItemCategory itemCategory) {
        return items.stream().filter(v -> itemCategory == v.getItemCategory()).collect(toList());
    }

    public static List<Item> getItems(List<Item> items, Cuisine cuisine, ItemCategory itemCategory) {
        return getItems(getItems(items, cuisine), itemCategory);
    }
}
